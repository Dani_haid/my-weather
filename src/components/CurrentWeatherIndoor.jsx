import React from "react";
import { useSelector } from 'react-redux';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import '../styles/CurrentWeather.css';
import IndoorNotification from './IndoorNotification';


const CurrentWeatherIndoor = () => {
    const weather = useSelector((state) => state.weather.weather_indoor)

    /*     splitting characters at . for different styling */
    const temp = weather.map((data) => data.I_temperature)
    const str_temp = temp.toString();
    const temp_splitted = str_temp.split('.')

    const hum = weather.map((data) => data.I_humidity)
    const str_hum = hum.toString();
    const hum_splitted = str_hum.split('.')


    const renderWeather = weather.map((data) => {
        return (
            <Container fluid key={data.EntryID}>
                <Row>
                    <Col>
                        <h2>Indoor:</h2>
                    </Col>
                </Row>
                <Row className='current-indoor-main'>
                    <Col>
                        <div className='current-indoor'>
                            <div className='indoor-icon'>
                                <svg id="Layer_1" enableBackground="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512" xmlns="http://www.w3.org/2000/svg"><g><path d="m234.501 317.738v-252.738c0-35.841-29.159-65-65-65s-65 29.159-65 65v252.738c-27.071 20.372-43 52.034-43 86.262 0 59.551 48.448 108 108 108s108-48.449 108-108c0-34.228-15.929-65.89-43-86.262zm-65 164.262c-43.01 0-78-34.991-78-78 0-26.762 13.486-51.355 36.075-65.787 4.314-2.756 6.925-7.521 6.925-12.641v-260.572c0-19.299 15.701-35 35-35s35 15.701 35 35v260.573c0 5.119 2.61 9.885 6.925 12.641 22.589 14.431 36.075 39.024 36.075 65.786 0 43.009-34.99 78-78 78z" /><path d="m184.501 361.58v-296.58c0-8.284-6.716-15-15-15s-15 6.716-15 15v296.58c-17.459 6.192-30 22.865-30 42.419 0 24.813 20.187 45 45 45s45-20.187 45-45c0-19.554-12.541-36.227-30-42.419zm-15 57.42c-8.271 0-15-6.729-15-15s6.729-15 15-15 15 6.729 15 15-6.729 15-15 15z" /><path d="m279.501 79.625c-8.284 0-15 6.716-15 15s6.716 15 15 15c34.118 0 61.876 27.758 61.876 61.877 0 34.118-27.758 61.876-61.876 61.876-8.284 0-15 6.716-15 15s6.716 15 15 15c50.66 0 91.876-41.216 91.876-91.876 0-50.662-41.216-91.877-91.876-91.877z" /><path d="m279.5 56c8.284 0 15-6.716 15-15v-25.999c0-8.284-6.716-15-15-15s-15 6.716-15 15v25.999c0 8.284 6.716 15 15 15z" /><path d="m279.5 287.001c-8.284 0-15 6.716-15 15v25.999c0 8.284 6.716 15 15 15s15-6.716 15-15v-25.999c0-8.284-6.716-15-15-15z" /><path d="m435.499 156.501h-25.998c-8.284 0-15 6.716-15 15s6.716 15 15 15h25.998c8.284 0 15-6.716 15-15s-6.716-15-15-15z" /><path d="m371.778 94.223c3.839 0 7.678-1.464 10.606-4.394l18.384-18.384c5.858-5.858 5.858-15.355 0-21.213-5.857-5.858-15.355-5.858-21.213 0l-18.384 18.384c-5.858 5.858-5.858 15.355 0 21.213 2.93 2.929 6.768 4.394 10.607 4.394z" /><path d="m382.031 252.818c-5.857-5.858-15.355-5.857-21.213 0-5.857 5.858-5.858 15.355 0 21.213l18.383 18.384c2.929 2.929 6.768 4.394 10.606 4.394s7.678-1.465 10.606-4.393c5.857-5.858 5.858-15.355 0-21.213z" /></g></svg>
                            </div>
                            <div className='current-value-wrapper'>
                                <span className='firstdigits'>{temp_splitted[0]}</span>
                                {temp_splitted[1] ?
                                    <span className='lastdigits'>.{temp_splitted[1]}</span>
                                    : <span className='lastdigits'>.0</span>}
                                <span className='unit'>°C</span>
                            </div>
                        </div>
                    </Col>
                    <Col>
                        <div className='current-indoor'>
                            <div className='indoor-icon'>
                                <svg id="Layer_1" enableBackground="new 0 0 511.998 511.998" viewBox="0 0 511.998 511.998" xmlns="http://www.w3.org/2000/svg"><g clipRule="evenodd" fillRule="evenodd"><path d="m457.922 150.376c-9.546-16.338-19.751-31.784-28.735-43.495-13.243-17.264-21.065-23.383-29.891-23.383-8.367 0-15.171 5.86-22.813 14.659-5.627 6.479-12.334 15.463-19.395 25.977-10.119 15.067-19.836 31.499-28.143 47.443-8.9-17.062-17.22-31.766-23.371-42.293-19.247-32.938-39.773-64.018-57.8-87.517-29.563-38.539-40.678-41.767-48.775-41.767s-19.212 3.228-48.777 41.768c-18.026 23.499-38.553 54.579-57.799 87.517-23.129 39.581-76.924 138.227-76.924 199.214 0 101.183 82.318 183.5 183.5 183.5 94.936 0 173.261-72.468 182.571-164.991 5.819 1.074 11.742 1.634 17.727 1.634 53.597 0 97.202-43.604 97.202-97.202 0-30.494-24.169-76.409-38.577-101.064zm-258.923 331.622c-84.64 0-153.5-68.859-153.5-153.5 0-35.731 25.593-101.728 68.462-176.539 38.265-66.775 72.381-109.712 85.038-120.3 12.657 10.588 46.772 53.524 85.037 120.3 42.869 74.812 68.463 140.808 68.463 176.539 0 84.641-68.86 153.5-153.5 153.5zm200.298-163.357c-5.938 0-11.782-.776-17.44-2.292-3.305-33.058-18.968-74.148-36.089-110.795 16.68-37.25 41.428-73.956 53.514-87.961 7.635 8.798 21.154 27.339 36.191 53.925 19.428 34.348 31.026 64.225 31.026 79.922 0 37.054-30.147 67.201-67.202 67.201z" /><path d="m240.182 257.167c-7.175-4.145-16.348-1.684-20.49 5.49l-67.366 116.682c-4.142 7.175-1.684 16.349 5.49 20.49 2.362 1.364 4.941 2.013 7.486 2.013 5.184 0 10.226-2.691 13.004-7.503l67.366-116.682c4.142-7.175 1.684-16.348-5.49-20.49z" /><path d="m161.5 286.998c0-8.262-6.738-15-15-15s-15 6.739-15 15c0 8.262 6.739 15 15 15 8.261.001 15-6.738 15-15z" /><path d="m246.5 356.998c-8.262 0-15 6.738-15 15s6.739 15 15 15c8.262 0 15-6.739 15-15 0-8.262-6.739-15-15-15z" /></g></svg>
                            </div>
                            <div className='current-value-wrapper'>
                                <span className='firstdigits'>{hum_splitted[0]}</span>
                                {hum_splitted[1] ?
                                    <span className='lastdigits'>.{hum_splitted[1]}</span>
                                    : <span className='lastdigits'>.0</span>}
                                <span className='unit'>%</span>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    });
    return (
        <div className='current-indoor-container'>
            {renderWeather}
            <IndoorNotification />
        </div>
    )
};

export default CurrentWeatherIndoor;