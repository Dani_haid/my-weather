import React from 'react';
import Container from 'react-bootstrap/Container';
import { useDispatch, useSelector } from "react-redux";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Forecast from './Forecast';
import { setMapMounted, setMapDisplayed } from '../redux/actions';
import HistoricalWeather from './HistoricalWeather';
import TempMap from './TempMap';

const ForecastHistoryContainer = () => {
    let display = useSelector((state) => state.helper.mapDisplayed);
    const dispatch = useDispatch();

    function handleClick(key) {
        if (key === 'map') {
            dispatch(setMapMounted(true))
            dispatch(setMapDisplayed())
        }
    }

    return (
        <Container fluid className='forecast-history-container'>
            <Tabs onSelect={(key) => handleClick(key)} defaultActiveKey="forecast" id="uncontrolled-tab-example">
                <Tab eventKey="forecast" title="Forecast">
                    <Forecast />
                </Tab>
                <Tab eventKey="history" title="History">
                    <HistoricalWeather />
                </Tab>
                <Tab eventKey="map" title="Weather Map">
                    <TempMap />
                </Tab>
            </Tabs>
        </Container>
    )
}

export default ForecastHistoryContainer;
