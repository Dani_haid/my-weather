import ForecastHourly from './ForecastHourly';
import ForecastDaily from './ForecastDaily';

const Forecast = () => {
    return (
        <div>
            <ForecastHourly />
            <ForecastDaily />
        </div>
    )
}

export default Forecast;
