import React from "react";
import Toast from 'react-bootstrap/Toast';
import { useSelector, useDispatch } from 'react-redux';
import { toggleToast } from '../redux/actions';

const ToastUpdate = () => {
    const show = useSelector(state => state.helper.showToast);
    const dispatch = useDispatch();

    return (
        <Toast show={show} onClose={() => dispatch(toggleToast(false))} delay={3000} autohide>
            <Toast.Body>Wetterdaten wurden aktualisiert</Toast.Body>
        </Toast>
    )
}

export default ToastUpdate;