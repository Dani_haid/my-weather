import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import axios from 'axios';
import { receiveForecastHourly } from '../redux/actions';
import ForecastHourlyListing from './ForecastHourlyListing';


const api = '955a2eb9018ae5c07686e727b3e207da';

const ForecastHourly = () => {
    const dispatch = useDispatch();

    const getForecastHourly = async () => {
        const url = 'https://api.openweathermap.org/data/2.5/forecast?id=7873627&cnt=3&units=metric&lang=de&exclude=daily&appid='
        const response = await axios.get(url + api).catch((err) => {
            console.log('Err', err);
        });
        dispatch(receiveForecastHourly(response.data.list));
    };

    useEffect(() => {
        getForecastHourly();
        const interval = setInterval(() => {
            getForecastHourly();
        }, 1800000); //refresh every 30minutes
    }, []);

    return (
        <div>
            <ForecastHourlyListing />
        </div>
    )
}

export default ForecastHourly;