import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import axios from 'axios';
import HistoricalGraphIndoor from "./HistoricalGraphIndoor";
import HistoricalGraphOutdoor from "./HistoricalGraphOutdoor";
import { receiveHistoryOutdoor, receiveHistoryIndoor } from "../redux/actions";

const HistoricalWeather = () => {
    const dispatch = useDispatch();

    // default value 14 days, if no value set, use default value
    const fetchOutdoorHistory = async (length) => {
        let default_length = 14;
        if (!length) {
            length = default_length;
        }
        const url = 'https://dani.retracked.net/historical_weather_outdoor.php'
        const response = await axios.post(url, { period: length }).catch((err) => {
            console.log('Err', err);
        });
    
    //splitting response object into separate arrays to provide for chart
        const categories = [];
        const data_avg = [];
        const data_min = [];
        const data_max = [];
        for (let i = 0; i < response.data.length; i++) {
            data_avg.push(response.data[i].average)
            data_min.push(response.data[i].minimum)
            data_max.push(response.data[i].maximum)
            categories.push(response.data[i].time)
        };
        dispatch(receiveHistoryOutdoor(categories, data_avg, data_min, data_max));
    };

    // default value 14 days, if no value set, use default value
    const fetchIndoorHistory = async (length) => {
        let default_length = 14;
        if (!length) {
            length = default_length;
        }

        const url = 'https://dani.retracked.net/historical_weather_indoor.php'
        const response = await axios.post(url, { period: length }).catch((err) => {
            console.log('Err', err);
        });

        //splitting response object into separate arrays to provide for chart
        const categories = [];
        const data_avg = [];
        const data_min = [];
        const data_max = [];
        for (let i = 0; i < response.data.length; i++) {
            data_avg.push(response.data[i].average)
            data_min.push(response.data[i].minimum)
            data_max.push(response.data[i].maximum)
            categories.push(response.data[i].time)
        };
        dispatch(receiveHistoryIndoor(categories, data_avg, data_min, data_max));
    };

    useEffect(() => {
        fetchOutdoorHistory();
        fetchIndoorHistory();

        let nextMidnight = new Date();
        nextMidnight.setHours(24, 0, 0, 0);
        //data will always be fetched and refreshed on midnight
        const interval = setInterval(() => {
            let now = new Date();
            let remainingTimeInSeconds = (nextMidnight.getTime() - now.getTime()) / 1000;

            if (remainingTimeInSeconds < 0) {
                fetchOutdoorHistory(14);
                fetchIndoorHistory(14);
                nextMidnight = new Date();
                nextMidnight.setHours(24, 0, 0, 0);
            }
        }, 3600000); //1 hour interval
    }, []);

    return (
        <div className='container_historical_weather'>
            <HistoricalGraphOutdoor fetchOutdoorHistory={fetchOutdoorHistory} />
            <HistoricalGraphIndoor fetchIndoorHistory={fetchIndoorHistory} />
        </div>
    )
}

export default HistoricalWeather;