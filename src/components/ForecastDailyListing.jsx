import React from "react";
import { useSelector } from "react-redux";
import Moment from 'react-moment';
import { Swiper, SwiperSlide } from 'swiper/react';
import '../styles/ForecastDaily.css';
import 'swiper/swiper-bundle.min.css';


const ForecastDailyListing = () => {
    let forecastDaily = useSelector((state) => state.weather.forecast_daily);

    //check if element has more than 6 values, if yes element will be splitted. If not it will stay the same
    if (forecastDaily.length >= 6) {
        forecastDaily.shift();
        forecastDaily.splice(4, 3);
    }

    const renderForecastDaily = forecastDaily.map((data) => {
        return (
            <SwiperSlide key={data.dt} >
                <div className='forecast-daily-card'>
                    <div className='forecast-daily-day'>
                        <Moment unix format='dddd'>{data.dt}</Moment>
                    </div>
                    <div className='forecast-daily-date light20'>
                        <Moment unix format='DD. MMM YYYY'>{data.dt}</Moment>
                    </div>
                    <div className='forecast-daily-icon'>
                        <img src={`img/weather_icons_full/${data.weather[0].icon}.svg`} alt='weather_icon'></img>
                    </div>
                    <div className='forecast-daily-temp-wrapper'>
                        <div className='forecast-daily-temp min light20'>{Math.round(data.temp.min)}°</div>
                        <div className='forecast-daily-temp max'>{Math.round(data.temp.max)}°</div>
                    </div>
                    <div className='light20'>UV: {data.uvi}</div>
                    <div className='light20'>Cloudiness: {data.clouds} %</div>
                    <div className='light20'>Rain: {data.rain ? (Math.round(data.rain * 10) / 10) : 0} mm</div>
                </div >
            </SwiperSlide>
        )
    })

    return (
        <div>
            <h3>Daily:</h3>
            <Swiper
                breakpoints={{
                    // when window width is >= 480px
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    },
                    // when window width is >= 980px
                    980: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    // when window width is >= 1200px
                    1200: {
                        slidesPerView: 4,
                        spaceBetween: 30
                    },
                }}
                spaceBetween={30}
                slidesPerView={1}
            >
                {renderForecastDaily}
            </Swiper>
        </div>
    )
}

export default ForecastDailyListing;