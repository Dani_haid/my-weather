import React, { useState } from "react";
import { useSelector } from "react-redux";
import Chart from "react-apexcharts";
import '../styles/HistoricalWeather.css';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


const HistoricalGraphOutdoor = ({ fetchOutdoorHistory }) => {
    const historyOutdoor = useSelector((state) => state.historyOutdoor.history_outdoor);

/*     handle active state of button with hooks */
    const [isActive, setActive] = useState(14);

    const handleClick = (e) => {
        const length = e.target.value;
        { fetchOutdoorHistory(length) };
        setActive(length)
    }

    return (
        <div className='container_graph'>
            <div>
                <h3>Outdoor:</h3>
                <div className='period-btn-wrapper'>
                    <Row>
                        <Col xs={6} sm={3}>
                            <Button size='sm' type='submit' name='7days' className={isActive == 7 ? 'active' : null} value='7' onClick={handleClick}>
                                7 Days
                    </Button>
                        </Col>
                        <Col xs={6} sm={3}>
                            <Button size='sm' type='submit' name='14days' className={isActive == 14 ? 'active' : null} value='14' onClick={handleClick}>
                                14 Days
                    </Button>
                        </Col>
                        <Col xs={6} sm={3}>
                            <Button size='sm' type='submit' name='30days' className={isActive == 30 ? 'active' : null} value='30' onClick={handleClick}>
                                30 Days
                    </Button>
                        </Col>
                        <Col xs={6} sm={3}>
                            <Button size='sm' type='submit' name='60days' className={isActive == 60 ? 'active' : null} value='60' onClick={handleClick}>
                                60 Days
                    </Button>
                        </Col>
                    </Row>
                </div>

                <div className='graph_wrapper'>
                    <Chart
                        options={historyOutdoor.options}
                        series={historyOutdoor.series}
                        type="line"
                        height='100%'
                    />
                </div>
            </div>
        </div>
    )
}


export default HistoricalGraphOutdoor;