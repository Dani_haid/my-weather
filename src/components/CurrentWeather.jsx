import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import axios from 'axios';
import { receiveOutdoorWeather, receiveIndoorWeather } from '../redux/actions';
import CurrentWeatherOutdoor from './CurrentWeatherOutdoor';
import CurrentWeatherIndoor from './CurrentWeatherIndoor';


const CurrentWeather = () => {
    const dispatch = useDispatch();

    const fetchOutdoorWeather = async () => {
        const url = 'https://dani.retracked.net/o_weather.php'
        const response = await axios.get(url).catch((err) => {
            console.log('Err', err);
        });
        dispatch(receiveOutdoorWeather(response.data));
    };

    const fetchIndoorWeather = async () => {
        const url = 'https://dani.retracked.net/i_weather.php'
        const response = await axios.get(url).catch((err) => {
            console.log('Err', err);
        });
        dispatch(receiveIndoorWeather(response.data));
    };

    useEffect(() => {
        fetchOutdoorWeather();
        fetchIndoorWeather();
        const interval = setInterval(() => {
            fetchOutdoorWeather();
            fetchIndoorWeather();
        }, 300000);//refresh every 5 Minutes
    }, [] );

    return (
        <div className='current-weather-wrapper'>
            <CurrentWeatherOutdoor updateOutdoor={fetchOutdoorWeather} updateIndoor={fetchIndoorWeather} />
            <CurrentWeatherIndoor />
        </div>
    )
}

export default CurrentWeather;