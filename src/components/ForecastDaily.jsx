import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import axios from 'axios'; 
import { receiveForecastDaily } from '../redux/actions';
import ForecastDailyListing from './ForecastDailyListing';


const api = '955a2eb9018ae5c07686e727b3e207da';

const ForecastDaily = () => {
    const dispatch = useDispatch();

          const getForecastDaily = async () => {
            const url = 'https://api.openweathermap.org/data/2.5/onecall?lat=47.2523&lon=11.3225&exclude=current,minutely,hourly,alert&units=metric&lang=de&appid='
            const response = await axios.get(url+api).catch((err) => {
                console.log('Err', err);
            });
            dispatch(receiveForecastDaily(response.data.daily));            
        };

         useEffect(() => {
            getForecastDaily();
            const interval = setInterval(() => {
                getForecastDaily();
            }, 7200000); //refresh every 2 hours
        }, [] ); 

    return (
        <div>
            <ForecastDailyListing />
        </div>
    )
}

export default ForecastDaily;