import React, { useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { setMapMounted } from '../redux/actions';
import '../styles/Map.css';
// Openlayers imports
import { Map, View } from 'ol';
import { Tile as TileLayer } from 'ol/layer';
import { XYZ as XYZSource } from 'ol/source';
import { defaults as DefaultInteractions } from 'ol/interaction';
import { ScaleLine, ZoomSlider, ZoomToExtent, defaults as DefaultControls } from 'ol/control'

const api = '955a2eb9018ae5c07686e727b3e207da'

const TempMap = () => {
    let load = useSelector((state) => state.helper.mapMount);
    let display = useSelector((state) => state.helper.mapDisplayed);
    const dispatch = useDispatch();

    /* init map (in Useeffect) */
    const initMap = () => {
        const map = new Map({
            interactions: DefaultInteractions({
                mouseWheelZoom: false,
                doubleClickZoom: false,
                shiftDragZoom: false,
                keyboard: false,
                pinchRotate: false,
                pinchZoom: false,
            }),
            target: 'ol-map',
            layers: [
                new TileLayer({
                    source: new XYZSource({
                        url: /*  'https://{a-c}.tile.openstreetmap.de/{z}/{x}/{y}.png', */
                            'https://api.maptiler.com/maps/pastel/{z}/{x}/{y}.png?key=PCvcunl0L0bgj6zesOuZ#0.3/-31.07212/45.81718',
                        projection: 'EPSG:3857',
                        attributions: 'Maptiler.com',
                        attributionsCollapsible: false,
                    }),
                    className: 'osm-layer'
                }),
                new TileLayer({
                    source: new XYZSource({
                        url: 'https://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png?appid=' + api,
                        projection: 'EPSG:3857',
                    }),
                    className: 'owp-layer',
                })
            ],
            controls: DefaultControls().extend([
                new ZoomSlider(),
                new ScaleLine(),
                new ZoomToExtent({
                    label: 'W',
                    extent: 0
                })
            ]),
            view: new View({
                projection: 'EPSG:3857',
                center: [1306155.939337, 5989624.645683],
                zoom: 5
            })
        })
    }
    const style = {
        width: '100%',
        height: '600px',
        backgroundColor: '#cccccc',
    }

    /* correctly load map in tab */
    useEffect(() => {
        if (load === true && display === 1) {
            initMap()
        } else {
        }
        return () => {
            dispatch(setMapMounted(false))
        };
    });

    return (
        <div className='container-weather-map'>
            <div id='ol-map' style={style} >
            </div>
            <div className='map-legend'></div>
        </div>
    )
}

export default TempMap;