import { ActionTypes } from "../constants/action-types";

export const receiveOutdoorWeather = (weather) => {
  return {
    type: ActionTypes.RECEIVE_OUTDOOR_WEATHER,
    payload: weather,
  };
};

export const receiveIndoorWeather = (weather) => {
  return {
    type: ActionTypes.RECEIVE_INDOOR_WEATHER,
    payload: weather,
  };
};

export const receiveForecastHourly = (weather) => {
  return {
    type: ActionTypes.RECEIVE_FORECAST_HOURLY,
    payload: weather,
  };
};

export const receiveForecastDaily = (weather) => {
  return {
    type: ActionTypes.RECEIVE_FORECAST_DAILY,
    payload: weather,
  };
};

export const receiveHistoryOutdoor = (categories, data_avg, data_min, data_max) => {
  return {
    type: ActionTypes.RECEIVE_HISTORY_OUTDOOR,
    payload: {
      categories,
      data_avg,
      data_min,
      data_max
    },
  };
};

export const receiveHistoryIndoor = (categories, data_avg, data_min, data_max) => {
  return {
    type: ActionTypes.RECEIVE_HISTORY_INDOOR,
    payload: {
      categories,
      data_avg,
      data_min,
      data_max
    },
  };
};

export const toggleToast = (show) => {
  return{
    type: ActionTypes.TOGGLE_TOAST_UPDATE,
    payload: show
  }
};

export const setMapMounted = (val) => {
  return{
    type: ActionTypes.SET_MAP_MOUNTED,
    payload: val
  }
};

export const setMapDisplayed = (val) => {
  return{
    type: ActionTypes.SET_MAP_DISPLAYED,
    payload: val
  }
};