import { combineReducers } from 'redux';
import { weatherReducer } from './weatherSlice';
import { historyReducerOutdoor } from './historyOutdoorSlice.js';
import { historyReducerIndoor } from './historyIndoorSlice.js';
import { helperReducer } from './helperSlice';

const reducers = combineReducers({
    weather : weatherReducer,
    historyOutdoor : historyReducerOutdoor,
    historyIndoor : historyReducerIndoor,
    helper : helperReducer,
});

export default reducers