
import { ActionTypes } from '../constants/action-types';

const initialState = {
    weather_outdoor: [],
    weather_indoor: [],
    forecast_hourly: [],
    forecast_daily: [],
};

export const weatherReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.RECEIVE_OUTDOOR_WEATHER:
            return { ...state, 
                weather_outdoor: payload 
            };
        case ActionTypes.RECEIVE_INDOOR_WEATHER:
            return {
                ...state,
                weather_indoor: payload
            };
        case ActionTypes.RECEIVE_FORECAST_HOURLY:
            return {
                ...state,
                forecast_hourly: payload
            };
        case ActionTypes.RECEIVE_FORECAST_DAILY:
            return {
                ...state,
                forecast_daily: payload
            };
        default:
            return state;
    }
};