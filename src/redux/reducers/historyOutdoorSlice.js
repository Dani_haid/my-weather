import { ActionTypes } from '../constants/action-types';

const initialState = {
    history_outdoor: {
        options: {
            chart: {
                id: "historical_outdoor",
                toolbar: {
                    show: false,
                },
                animations: {
                    enabled: true,
                    easing: 'easeinout',
                    speed: 800,
                    animateGradually: {
                        enabled: true,
                        delay: 150
                    },
                    dynamicAnimation: {
                        enabled: true,
                        speed: 350
                    }
                },
                zoom: {
                    enabled: false,
                }
            },
            colors: ['#23add2', '#495abf', '#7989f1'],
            dataLabels: {
                enabled: false
            },
            grid: {
                show: true,
                borderColor: '#73767f',
                strokeDashArray: 5,
            },

            legend: {
                fontSize: '12px',
                labels: {
                    colors: '#73767f',
                },
                markers: {
                    width: 5,
                    height: 5,
                },
                itemMargin: {
                    horizontal: 10,
                    vertical: 20,
                }
            },
            xaxis: {
                categories: [],
                type: 'datetime',
                labels: {
                    datetimeFormatter: {
                        day: 'dd.MM'
                    }
                },
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
                tooltip: {
                    enabled: false,
                }
            },
            yaxis: {
                forceNiceScale: true,
                labels: {
                    formatter: function (val) {
                        return (Math.round(val)) + ' °C';
                    }
                },
            },
            //styled in css
            tooltip: {
                fillSeriesColor: false,
                x: {
                    format: 'dd/MM/yyyy'
                },
                y: {
                    formatter: function (value, { series, seriesIndex, dataPointIndex, w }) {
                        return value + ' °C'
                    },
                }
            },
        },
        series: [
            {
                name: "Maximal Temp.",
                data: []
            },
            {
                name: "Avg. Temp.",
                data: []
            },
            {
                name: "Minimal Temp",
                data: []
            },
        ],
    },
};

export const historyReducerOutdoor = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.RECEIVE_HISTORY_OUTDOOR:
            return {
                ...state,
                history_outdoor: {
                    options: {
                        chart: {
                            id: "historical_weather_outdoor"
                        },
                        xaxis: {
                            categories: payload.categories
                        }
                    },
                    series: [
                        {
                            name: "Maximal Temp.",
                            data: payload.data_max
                        },
                        {
                            name: "Avg. Temp.",
                            data: payload.data_avg
                        },
                        {
                            name: "Minimal Temp.",
                            data: payload.data_min
                        },
                    ]
                },
            };
        default:
            return state;
    }
};