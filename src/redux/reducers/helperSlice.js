
import { ActionTypes } from '../constants/action-types';

const initialState = {
    showToast: false,  
    mapMount: false,
    mapDisplayed: 0
};

export const helperReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.TOGGLE_TOAST_UPDATE:
            return { ...state, 
                showToast: payload
            };

        case ActionTypes.SET_MAP_MOUNTED:
            return{ ...state,
                mapMount: payload,
            };

        case ActionTypes.SET_MAP_DISPLAYED:
            return{...state,
                mapDisplayed: state.mapDisplayed +1,
            }
        default:
            return state;
    }
};