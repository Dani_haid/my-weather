import CurrentWeather from './components/CurrentWeather';
import ForecastHistoryContainer from './components/ForecastHistoryContainer';
import Sun from './components/SunriseSunset';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/App.css';
import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const App = () => {

  /* dynamically update favicon */
  const weather = useSelector((state) => state.weather.weather_outdoor);
  useEffect(() => {
    const faviconUpdate = async () => {
      const icon = weather.map((data) => data.O_icon)
      const favicon = document.getElementById('favicon');

      if (icon) {
        favicon.href = `/img/weather_icons_flat/${icon}.svg`;
      }
    };
    faviconUpdate();
  }, [weather])

  return (
    <div className='weather-wrapper'>
      <CurrentWeather />
      <div className='more-info-wrapper'>
        <Sun />
        <ForecastHistoryContainer />
      </div>
    </div>
  )
}


export default App;
